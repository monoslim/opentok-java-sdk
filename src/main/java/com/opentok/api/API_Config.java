/*!
* OpenTok Java Library
* http://www.tokbox.com/
*
* Copyright 2010, TokBox, Inc.
*
*/
package com.opentok.api;

public class API_Config {
	public static int API_KEY = 0; // Fill this in with generated API Key (http://tokbox.com/opentok/api/tools/js/apikey)
	
	public static String API_SECRET = ""; // Fill this in with generated API Secret in email
	
	public static String API_URL = "http://api.opentok.com";
}

